<?php

date_default_timezone_set('Europe/Berlin');
	
if($_POST){
	if ($_POST['action'] == 'addSighting') {
	
		$datum = $_POST['sichtungsdatum'] ;
		$typ =        htmlspecialchars($_POST['art_der_kreatur']);
		$entfernung = htmlspecialchars($_POST['entf_kreatur']);
		$gewicht =    htmlspecialchars($_POST['gew_kreatur']);
		$hoehe =      htmlspecialchars($_POST['hoehe_kreatur']);
		$farbe =      htmlspecialchars($_POST['farbe_kreatur_rgb']);
		$breite =     htmlspecialchars($_POST['sichtung_breite']);
		$laenge =     htmlspecialchars($_POST['sichtung_laenge']);
		
		$mein_datum = date('Y-m-d', strtotime($datum));
		
		if($typ == ''){
			$typ = "Andere";
		}
		
		$abfrage = "INSERT INTO sightings (sighting_date, creature_type, creature_distance, creature_weight, creature_height, creature_color, sighting_latitude, sighting_longitude) ";
		$abfrage .= "VALUES ('$mein_datum', '$typ', '$entfernung', '$gewicht', '$hoehe', '$farbe', '$breite', '$laenge') ";

		$ergebnis = datenbank_verbindung($abfrage);
		
		if ($ergebnis) {
			$nachricht = "Kreatur erfolgreich hinzugefügt";
			erfolg($nachricht);
		} else {
			fehler('Kein Einfügen möglich.');
		}
		exit;
	}
}	

if($_GET){
	if($_GET['action'] == 'alle_sichtungen_abfragen'){
		$abfrage  = "SELECT sighting_id, sighting_date, creature_type FROM sightings order by sighting_date ASC ";
		$ergebnis = datenbank_verbindung($abfrage);
		
		$sichtungen = array();

		while ($zeile = mysql_fetch_array($ergebnis, MYSQL_ASSOC)) {
			array_push($sichtungen, array('id' => $zeile['sighting_id'], 'datum' => $zeile['sighting_date'], 'typ' => $zeile['creature_type'] ));
		}
		echo json_encode(array("sichtungen" => $sichtungen));
		exit;	
	}elseif($_GET['action'] == 'einzelne_sichtung_abfragen'){
		$id = htmlspecialchars($_GET['id']);
		$abfrage  = "SELECT sighting_date, creature_type, creature_distance, creature_weight, creature_height, creature_color, sighting_latitude, ";
		$abfrage .= " sighting_longitude FROM sightings where sighting_id = '$id' ";
		$ergebnis = datenbank_verbindung($abfrage);
		
		$sichtungen = array();

		while ($zeile = mysql_fetch_array($ergebnis, MYSQL_ASSOC)) {
			array_push($sichtungen, array('datum' => $zeile['sighting_date'], 'typ' => $zeile['creature_type'], 'entfernung' => $zeile['creature_distance'], 'gewicht' => $zeile['creature_weight'],
					'hoehe' => $zeile['creature_height'], 'farbe' => $zeile['creature_color'], 'breite' => $zeile['sighting_latitude'], 'laenge' => $zeile['sighting_longitude']
				)
			);
		}
		echo json_encode(array("sichtungen" => $sichtungen));
		exit;	
		
	} elseif($_GET['action'] == 'sichtungen_nach_typ_abfragen') {
		$typ      = htmlspecialchars($_GET['typ']);
		$abfrage  = "SELECT sighting_id, sighting_date, creature_type, creature_distance, creature_weight, creature_height, creature_color, sighting_latitude, ";
		$abfrage .= " sighting_longitude FROM sightings where creature_type = '$typ' order by sighting_date ASC ";
		$ergebnis = datenbank_verbindung($abfrage);
		
		$sichtungen = array();

		while ($zeile = mysql_fetch_array($ergebnis, MYSQL_ASSOC)) {
			array_push($sichtungen, array('id' => $zeile['sighting_id'], 'datum' => $zeile['sighting_date'], 'typ' => $zeile['creature_type'], 'entfernung' => $zeile['creature_distance'], 'gewicht' => $zeile['creature_weight'], 
					'hoehe' => $zeile['creature_height'], 'farbe' => $zeile['creature_color'], 'breite' => $zeile['sighting_latitude'], 'laenge' => $zeile['sighting_longitude']
				)
			);
		}
		echo json_encode(array("sichtungen" => $sichtungen));
		exit;
		
	} elseif($_GET['action'] == 'sichtungs_typen_abfragen') {
		$abfrage  = "SELECT distinct(creature_type) FROM sightings order by creature_type ASC ";
		$ergebnis = datenbank_verbindung($abfrage);
		
		$typen = array();

		while ($zeile = mysql_fetch_array($ergebnis, MYSQL_ASSOC)) {
			array_push($typen, array('typ' => $zeile['creature_type']));
		}
		echo json_encode(array("kreatur_typen" => $typen));
		exit;
	} else {}
}

// Hilfsfunktionen

	function datenbank_verbindung($abfrage) {
		mysql_connect('pumsche.pu.funpicsql.de', 'pumsche', 'Shauni1806')
			OR die(fehler('Keine Verbindung zur Datenbank möglich.'));
		mysql_select_db('pumsche');

		return mysql_query($abfrage);
	}
	
	function fehler($json_nachricht) {
		die(json_encode(array('status' => 'fehler', 'nachricht' => $json_nachricht)));
	}
	function erfolg($json_nachricht) {
		die(json_encode(array('status' => 'erfolg', 'nachricht' => $json_nachricht)));
	}
?>