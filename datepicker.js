$(document).ready(function(){
    

       
   //1. Datumswähler erstellen, über den die Benutzer das Datum der Sichtung eingeben können.
		$("#datumsauswahl").datepicker({   stepMonths:1,changeMonth: true, changeYear: true});


	//2. Bessere Radiobuttons erstellen, über die Benutzer die Art der Kreatur auswählen können.
		$( "#typ_auswahl" ).buttonset();
		

 
   
  
   
   $('#radio').buttonset();
  
   
 
 $('#entf_schieber').slider({
    slide:function(event,ui){
           $('#entfernung').val(ui.value);
           
       
       $('#entf_schieber_wert').val($('#entf_schieber').slider("value"));
   
    }
    });
    $('#schieber_gewicht').slider({
      
      
     value:0,
     min:0,
     max:500,
     step:5,
     orientation:'horizontal'});
 $('#schieber_gewicht').slider({
    slide:function(event,ui){
           $('#gewicht').val(ui.value);
           
       
       $('#schieber_gewicht_wert').val($('#schieber_gewicht').slider("value"));
   
    }
    });
    $('#schieber_hoehe').slider({
      
      
     value:0,
     min:0,
     max:500,
     step:20,
     orientation:'horizontal'});
 $('#schieber_hoehe').slider({
    slide:function(event,ui){
           $('#hoehe').val(ui.value);
           
       
       $('#schieber_hoehe_wert').val($('#schieber_hoehe').slider("value"));
   
    }
    });
    
     $('#schieber_laenge').slider({
      
      
     value:0,
     min:-90,
     max:90,
     step:0.00001,
     orientation:'horizontal'});
 $('#schieber_laenge').slider({
    slide:function(event,ui){
           $('#laenge').val(ui.value);
           
       
       $('#schieber_laenge_wert').val($('#schieber_laenge').slider("value"));
   
    }
    });
    
     $('#schieber_breite').slider({
      
      
     value:0,
     min:-90,
     max:90,
     step:0.00001,
     orientation:'horizontal'});
 $('#schieber_breite').slider({
    slide:function(event,ui){
           $('#breite').val(ui.value);
           
       
       $('#schieber_breite_wert').val($('#schieber_breite').slider("value"));
   
    }
    });
    
    function palette_aktualisieren() {

		var	rot =   $( "#rot" ).slider( "value" );
		var	gruen = $( "#gruen" ).slider( "value" );
		var	blau =  $( "#blau" ).slider( "value" );
		var	mein_rgb = "rgb(" + rot + "," + gruen + "," + blau + ")"; 
			
			$( "#palette" ).css( "background-color", mein_rgb );
			$( "#rot_wert" ).val(rot );
			$( "#blau_wert" ).val( blau);
			$( "#gruen_wert" ).val( gruen);
			$( "#farb_wert" ).val( mein_rgb);		
	}

	$( "#rot, #gruen, #blau" ).slider({
		orientation: "horizontal",	
		range: "min", 
		max: 255, 
		value: 127, 
		slide: palette_aktualisieren,
		change: palette_aktualisieren
	});
								
	

	$( "#rot" ).slider( "value", 127 );
	$( "#gruen" ).slider( "value", 127 );
	$( "#blau" ).slider( "value", 127 );
        $('button:submit').button();
    
    $("#formular_neue_sichtung").submit(function(){
		return false;
	});
	
	$('#sichern_button').click(function() {

		var data = $("#formular_neue_sichtung :input").serializeArray();

		$.post($("#formular_neue_sichtung").attr('action'), data, function(json){
			
			if (json.status == "fehler") {
				alert(json.nachricht);
			}else if (json.status == "erfolg") {
				alert(json.nachricht);
			}else{alert("Nichts passiert. Das ist seltsam.");}
		}, "json");

	});	
    
    
});