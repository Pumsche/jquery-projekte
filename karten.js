$(document).ready(function(){

	var map;
	var info_fenster     = new google.maps.InfoWindow({content: ''});
	var marker_array     = [];
	var kartenausschnitt = new google.maps.LatLngBounds();

	function initialisieren(){
		var breite = 45.519098;
		var laenge = -122.672138;
		
		var breite_laenge = new google.maps.LatLng(breite,laenge);
		
		var mapOpts = {
			zoom:      13,
			center:    breite_laenge,
			mapTypeId: google.maps.MapTypeId.HYBRID
		};
		map = new google.maps.Map(document.getElementById("karten_container"), mapOpts);
                alle_sichtungen_abfragen();
        }
        
        
       function alle_sichtungen_abfragen(){
    	$.getJSON("service_en.php?action=alle_sichtungen_abfragen", function(json) {
			if (json.sichtungen.length > 0) {
				$("#sichtungs_liste").empty();	
				
				$.each(json.sichtungen,function() {
					var info = 'Datum: ' +  this['datum'] + ', Typ: ' +  this['typ'];
					
					var $li = $("<li/>");
					$li.html(info);
					$li.addClass("sichtungen");
					$li.attr('id', this['id']) ;
                                        $li.click(function(){
                                            einzelne_sichtung_abfragen(this['id']);
                                        });
					
					$li.appendTo("#sichtungs_liste");	
				});
			}
		});
    }
    
   function einzelne_sichtung_abfragen(id){
    	$.getJSON("service_en.php?action=einzelne_sichtung_abfragen&id="+id, function(json) {
			if (json.sichtungen.length > 0) {
				
				$.each(json.sichtungen,function() {
					var ort = new google.maps.LatLng(this['breite'], this['laenge']);
				
					var my_marker = new google.maps.Marker({
						position: ort, 
						map:      map,
						title:    this['typ'] 
					}); 
					map.setCenter(ort, 20);
				});
			}
		});	
	}
   initialisieren();
   
   

});
