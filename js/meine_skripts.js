$(document).ready(function(){

	//1. Datumswähler erstellen, über den die Benutzer das Datum der Sichtung eingeben können.
		$("#datumsauswahl").datepicker({ changeMonth: true, changeYear: true});


	//2. Bessere Radiobuttons erstellen, über die Benutzer die Art der Kreatur auswählen können.
		$( "#typ_auswahl" ).buttonset();
		
	//3. Mehrere Schieberegler für, Entfernung, Gewicht und Höhe der Kreatur definieren.

		//3a. Entfernung von der Kreatur
		$( "#entf_schieber" ).slider({
			value: 0,
			min:   0,
			max:   500,
			step:  1,
			slide: function( event, ui ) {
				$( "#entfernung" ).val( ui.value);
			}
		});
	$( "#entfernung" ).val( $( "#entf_schieber" ).slider( "value" ));
		
		
		
		//3b. Gewicht der Kreatur
		
		$( "#schieber_gewicht" ).slider({
			value: 0,
			min:   0,
			max:   5000,
			step:  5,
			slide: function( event, ui ) {
				$( "#gewicht" ).val( ui.value);
			}
		});
		
		$( "#gewicht" ).val( $( "#schieber_gewicht" ).slider( "value" ));
        
		//3c.  Höhe der Kreatur
		$( "#schieber_hoehe" ).slider({
			value: 0,
			min:   0,
			max:   20,
			step:  1,
			slide: function( event, ui ) {
				$( "#hoehe" ).val( ui.value);
			}
		});
		
        $( "#hoehe" ).val( $( "#schieber_hoehe" ).slider( "value" ));
        
		//3d. Geografische Breite
		
		$( "#schieber_breite" ).slider({
			value: 0,
			min:   -90,
			max:   90,
			step:  0.00001,
			slide: function( event, ui ) {
				$( "#breite" ).val( ui.value);
			}
		});
		
			//3e. Geografische Länge
		
		$( "#schieber_laenge" ).slider({
			value: 0,
			min:   -180,
			max:   180,
			step:  0.00001,
			slide: function( event, ui ) {
				$( "#laenge" ).val( ui.value);
			}
		});

	//4. Einen Farbwähler erstellen, über den Benutzer die Farbe der Kreatur intuitiver eingeben können.
	
	function palette_aktualisieren() {

		var	rot =   $( "#rot" ).slider( "value" );
		var	gruen = $( "#gruen" ).slider( "value" );
		var	blau =  $( "#blau" ).slider( "value" );
		var	mein_rgb = "rgb(" + rot + "," + gruen + "," + blau + ")"; 
			
			$( "#palette" ).css( "background-color", mein_rgb );
			$( "#rot_wert" ).val(rot );
			$( "#blau_wert" ).val( blau);
			$( "#gruen_wert" ).val( gruen);
			$( "#farb_wert" ).val( mein_rgb);		
	}

	$( "#rot, #gruen, #blau" ).slider({
		orientation: "horizontal",	
		range: "min", 
		max: 255, 
		value: 127, 
		slide: palette_aktualisieren,
		change: palette_aktualisieren
	});
						
	

	$( "#rot" ).slider( "value", 127 );
	$( "#gruen" ).slider( "value", 127 );
	$( "#blau" ).slider( "value", 127 );



	//5. Einen besser aussehenden "Abschicken"-Button für das Formular erstellen.
	$("button:submit").button();
		
	$("#formular_neue_sichtung").submit(function(){
		return false;
	});
	
	$('#sichern_button').click(function() {

		var data = $("#formular_neue_sichtung :input").serializeArray();

		$.post($("#formular_neue_sichtung").attr('action'), data, function(json){
			
			if (json.status == "fehler") {
				alert(json.nachricht);
			}else if (json.status == "erfolg") {
				alert(json.nachricht);
			}else{alert("Nichts passiert. Das ist seltsam.");}
		}, "json");

	});	
	
});//end doc ready
