$(document).ready(function(){



	var benutzte_karten = new Array();
	
	function karte(name,farbe,wert) {
		this.name  = name;
		this.farbe = farbe;
		this.wert  = wert;
	} 
	
  var stapel = [
    new karte('Ass', 'Kreuz',11),
    new karte('Zwei', 'Kreuz',2),
    new karte('Drei', 'Kreuz',3),
    new karte('Vier', 'Kreuz',4),
    new karte('Fuenf', 'Kreuz',5),
    new karte('Sechs', 'Kreuz',6),
    new karte('Sieben', 'Kreuz',7),
    new karte('Acht', 'Kreuz',8),
    new karte('Neun', 'Kreuz',9),
    new karte('Zehn', 'Kreuz',10),
    new karte('Bauer', 'Kreuz',10),
    new karte('Dame', 'Kreuz',10),
    new karte('Koenig', 'Kreuz',10),
    new karte('Ass', 'Herz',11),
    new karte('Zwei', 'Herz',2),
    new karte('Drei', 'Herz',3),
    new karte('Vier', 'Herz',4),
    new karte('Fuenf', 'Herz',5),
    new karte('Sechs', 'Herz',6),
    new karte('Sieben', 'Herz',7),
    new karte('Acht', 'Herz',8),
    new karte('Neun', 'Herz',9),
    new karte('Zehn', 'Herz',10),
    new karte('Bauer', 'Herz',10),
    new karte('Dame', 'Herz',10),
    new karte('Koenig', 'Herz',10),
    new karte('Ass', 'Pik',11),
    new karte('Zwei', 'Pik',2),
    new karte('Drei', 'Pik',3),
    new karte('Vier', 'Pik',4),
    new karte('Fuenf', 'Pik',5),
    new karte('Sechs', 'Pik',6),
    new karte('Sieben', 'Pik',7),
    new karte('Acht', 'Pik',8),
    new karte('Neun', 'Pik',9),
    new karte('Zehn', 'Pik',10),
    new karte('Bauer', 'Pik',10),
    new karte('Dame', 'Pik',10),
    new karte('Koenig', 'Pik',10),
    new karte('Ass', 'Karo',11),
    new karte('Zwei', 'Karo',2),
    new karte('Drei', 'Karo',3),
    new karte('Vier', 'Karo',4),
    new karte('Fuenf', 'Karo',5),
    new karte('Sechs', 'Karo',6),
    new karte('Sieben', 'Karo',7),
    new karte('Acht', 'Karo',8),
    new karte('Neun', 'Karo',9),
    new karte('Zehn', 'Karo',10),
    new karte('Bauer', 'Karo',10),
    new karte('Dame', 'Karo',10),
    new karte('Koenig', 'Karo',10)
  ];
	
	var blatt = {
		karten : new Array(),
		zwischensumme : 0,
		
		kartenwert_gesamt: function(){
			this.zwischensumme = 0;
			for(var i=0;i<this.karten.length;i++){
				var c = this.karten[i];
				this.zwischensumme += c.wert;
			}
			$("#ueber_gesamt").html("Gesamt: " + this.zwischensumme );
			
			if(this.zwischensumme > 21){
				$("#keine_karte_button").trigger("click");
				$("#img_ergebnis").attr('src','pixx/verloren.png');
				$("#ueber_ergebnis").html("ÜBERKAUFT!")
							   .attr('class', 'verloren');
			}else if(this.zwischensumme == 21){
				$("#keine_karte_button").trigger("click");
				$("#img_ergebnis").attr('src','pixx/gewonnen.png');
				$("#ueber_ergebnis").html("Black Jack!")
							   .attr('class', 'gewonnen');
			}else if(this.zwischensumme <= 21 && this.karten.length == 5){
				$("#keine_karte_button").trigger("click");
				$("#img_ergebnis").attr('src','pixx/gewonnen.png');
				$("#ueber_ergebnis").html("BlackJack - 5-Karten-Trick!")
							   .attr('class', 'gewonnen');
			}else{ }
		}
	};
	
	function zufallszahl_erzeugen(zahl){
		var zufallszahl = Math.floor(Math.random()*zahl);
		return zufallszahl;
	}
	
	// geben
	function karten_geben(){
		for(var i=0;i<2;i++){
			neue_karte();
		}
	}
	
	// neue Karte
	function neue_karte(){
		var unbenutzte_karte = false;
		do{
			var index = zufallszahl_erzeugen(52);
			if( !$.inArray(index, benutzte_karten ) > -1 ){
				unbenutzte_karte = true;
				var c = stapel[ index ];
				benutzte_karten[benutzte_karten.length] = index;
				blatt.karten[blatt.karten.length] = c;	
				
				var $d = $("<div>");
				$d.addClass("aktuelles_blatt")
				  .appendTo("#mein_blatt");
						  
				$("<img>").attr('alt',   c.farbe + ' ' + c.name) // z.B. "Pik Bube"
						  .attr('title', c.farbe + ' ' + c.name)
						  .attr('src', 'pixx/karten/' + c.farbe + '/' + c.name + '.jpg' )
						  .appendTo($d)
						  .fadeOut('slow')
						  .fadeIn('slow');
				
			}
		}while(!unbenutzte_karte);
		unbenutzte_karte = false;	  
		blatt.kartenwert_gesamt();
	}
	
	$("#geben_button").click( function(){
		karten_geben();
		$(this).toggle();
		$("#neue_karte_button").toggle();
		$("#keine_karte_button").toggle();
	});
	
	$("#neue_karte_button").click( function(){
		neue_karte();
	});
	
	
	
	function beenden(){
		$("#neue_karte_button").toggle();
		$("#keine_karte_button").toggle();
		$("#neustart_button").toggle();
	}
	
	$("#keine_karte_button").click( function(){
		$("#ueberschrift_ergebnis").html('Keine Karte!')
					   .attr('class', 'gewonnen');
		$("#ergebnis").toggle();
		beenden();
	});
	
	$("#neustart_button").click( function(){
		$("#ergebnis").toggle();
		$(this).toggle();
		$("#mein_blatt").empty();
		$("#ueberschrift_ergebnis").html('');
		$("#img_ergebnis").attr('src','bilder/gewonnen.png');
		
		benutzte_karten.length = 0;
		blatt.karten.length = 0;
		blatt.zwischensumme = 0;
		
		$("#geben_button").toggle()
					 .trigger('click');
	});
        
        
        
        
	});
