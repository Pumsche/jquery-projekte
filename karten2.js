 $(document).ready(function(){

	var karte;
	var info_fenster     = new google.maps.InfoWindow({content: ''});
	var marker_array     = [];
	var kartenausschnitt = new google.maps.LatLngBounds();

	function initialisieren(){
		var breite = 45.519098;
		var laenge = -122.672138;
		
		var breite_laenge = new google.maps.LatLng(breite,laenge);
		/* A.d.Ü.: Die Namen der Optionen in diesem Objekt werden von Google Maps 
		   auf englisch erwartet. Die Werte sind teilweise unsere Variablen */
		var karten_optionen = {
			zoom:      13,
			center:    breite_laenge,
			mapTypeId: google.maps.MapTypeId.HYBRID
		};
		karte = new google.maps.Map(document.getElementById("karten_container"), karten_optionen);
		
		if ( $('#typen_liste').length ) {
			// Aufklappmenü mit Inhalt füllen
			alle_typen_abfragen();
		}else{
			// Liste der Sichtungen mit Inhalt füllen
			alle_sichtungen_abfragen();
		}
    }
    
    function alle_sichtungen_abfragen(){
    	$.getJSON("service.php?action=alle_sichtungen_abfragen", function(json) {
			if (json.sichtungen.length > 0) {
				$("#sichtungs_liste").empty();	
				
				$.each(json.sichtungen,function() {
					var info = 'Datum: ' +  this['datum'] + ', Typ: ' +  this['typ'];
					
					var $li = $("<li />");
					$li.html(info);
					$li.addClass("sichtungen");
					$li.attr('id', this['id']) ;
					$li.click(function(){
						einzelne_sichtung_abfragen( this['id'] );		
					});
					$li.appendTo("#sichtungs_liste");	
				});
			}
		});
    }
	
	function einzelne_sichtung_abfragen(id){
    	$.getJSON("service.php?action=einzelne_sichtung_abfragen&id="+id, function(json) {
			if (json.sichtungen.length > 0) {
				
				$.each(json.sichtungen,function() {
					var ort = new google.maps.LatLng(this['breite'], this['laenge']);
				
					var my_marker = new google.maps.Marker({
						position: ort, 
						map:      karte,
						title:    this['typ'] 
					}); 
					karte.setCenter(ort, 20);
				});
			}
		});	
	}

    function alle_typen_abfragen(){
    	$.getJSON("service.php?action=sichtungs_typen_abfragen", function(json_types) {
			if (json_types.kreatur_typen.length > 0) {
				
				$.each(json_types.kreatur_typen,function() {
					var info =  this['typ'];
					var $li = $("<option />");
					$li.html(info);
					$li.appendTo("#typen_liste");	
				});
			}
		});
    }	

    function sichtungen_nach_typ_abfragen(typ){
    	$.getJSON("service.php?action=sichtungen_nach_typ_abfragen&typ="+typ, function(json) {
			if (json.sichtungen.length > 0) {
				$('#sichtungs_liste').empty();	
				
				$.each(json.sichtungen,function() {
					sichtung_hinzufuegen(this);
				});
				karte.fitBounds(kartenausschnitt);
			}
		});
    }	
    
    $('#typen_liste').change(function() {
    	if($(this).val() != ""){
  			overlays_loeschen();
  			sichtungen_nach_typ_abfragen( $(this).val() );
  		}
	});
	
	function sichtung_hinzufuegen(cryptid) {
	
		var ort = new google.maps.LatLng(cryptid['breite'], cryptid['laenge']);
		
		var info = 'Entfernung: ' +  cryptid['entfernung'] + 'm<br>';
		info += ' Höhe: ' +  cryptid['hoehe'] + 'm, Gewicht: ' +  cryptid['gewicht'] + 'kg, Farbe: ' +  cryptid['farbe'] + '<br>';
		info += 'Breitengrad: ' +  cryptid['breite'] + ', Längengrad: ' +  cryptid['laenge'];
		
		var opts = {
			map:       karte, 
			position:  ort, 
			clickable: true
		};
		
		var marker = new google.maps.Marker(opts);
		marker.note = 'Datum: ' +  cryptid['datum'] + ', Typ: ' +  cryptid['typ'];
		marker_array.push(marker);
		google.maps.event.addListener(marker, 'click', function() {
			info_fenster.content = info;
			info_fenster.open(karte, marker);
		});
		
		var $li = $("<li />");
		$li.html('Datum: ' +  cryptid['datum'] + ', Typ: ' +  cryptid['typ']);
		$li.addClass("sichtungen");
		$li.click(function(){
			info_fenster.content = info;
			info_fenster.open(karte, marker);		
		});
		$li.appendTo("#sichtungs_liste");	
		kartenausschnitt.extend(ort);
		return marker;
	}
	
	function overlays_loeschen() {
		if (marker_array) {
			for (i in marker_array) {
				marker_array[i].setMap(null);
			}
			marker_array.length = 0;
			kartenausschnitt = null;
			kartenausschnitt = new google.maps.LatLngBounds();
		}
	}

    initialisieren();
    
});