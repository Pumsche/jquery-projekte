$(document).ready(function(){

	var FREQ = 10000 ;
	var wiederholen = true;
	
	function frequenz_zeigen(){
		$("#freq").html( "Seite wird alle " + FREQ/1000 + " Sekunden automatisch neu geladen.");
	}
	
	function AJAX_Aufrufe_starten(){
	
		//if(wiederholen){
			setTimeout( function() {
					XML_lauefer_finden();
					AJAX_Aufrufe_starten();
				}, 	
				FREQ
			);
		//}
	}
	
	function XML_lauefer_finden(){
		$.ajax({
			url: "finisher.xml",
			cache: false,
			dataType: "xml",
			success:  function(xml){
				
				$('#finisher_m').empty();
				$('#finisher_w').empty();
				$('#alle_finisher').empty();
				
				$(xml).find("runner").each(function() {
					var info = '<li>Name: ' + $(this).find("fname").text() + ' ' + $(this).find("lname").text() + '. Time: ' + $(this).find("time").text() + '</li>';
					if( $(this).find("gender").text() == "m" ){
						$('#finisher_m').append( info );
					}else if ( $(this).find("gender").text() == "f" ){
						$('#finisher_w').append( info );
					}else{  }
					$('#alle_finisher').append( info );
				});
				
				aktuelle_zeit_ajax();
			}
		});
	}

	function aktuelle_zeit_ajax(){
	$('#letzte_aktualisierung').load("time.php");
		
		var time = "";
		$.ajax({
			url: "zeit.php",
			cache: false,
			success: function(data){
				$('#letzte_aktualisierung').html(data);
			}
		});
		
	}
	
	$("#stopp_button").click(function(){
		wiederholen = false;
		$("#freq").html( "Aktualisierung angehalten." );
	});

	$("#start_button").click(function(){
		wiederholen = true;
		AJAX_Aufrufe_starten();
		frequenz_zeigen();
	});	

	frequenz_zeigen();
	XML_lauefer_finden();
	AJAX_Aufrufe_starten();
        
        
});
